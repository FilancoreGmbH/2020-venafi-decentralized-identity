![](docFiles/filancore_logo.png)

# **VENAFI** ![](docFiles/venafi_logo.png) **<-> Verifiable Credentials**

# **Installation**

## Prerequisites

- [Python](https://www.python.org/) 3.7+
- All prerequisites for the [VENAFI VCert Python Library](https://github.com/Venafi/vcert-python#prerequisites-for-using-with-trust-protection-platform)
- `pip install -r requirements.txt`

## Configuration

In order for this library to work with your setup, you have to configure it properly within this file:

- `config.json` holds the configuration for your TPP/Cloud Platform




## Context

Industry 4.0 and IoT are currently among the most important topics for the further development and optimization of industrial value creation networks. The terms stand for the vision of an intelligent factory or device that can quickly adapt dynamically to new tasks and that goes hand in hand with the digitization of the economy. A requirement of the intelligent factory is that machines or devices are interconnected. This results in corresponding threats to the industrial production facilities. Hence, these facilities must be adequately protected, meaning that IT security becomes a critical factor to these systems. The trend in Industry 4.0 and IoT encourages the networking of machinery and devices, which make the attack surfaces grow even more. Businesses of all size in manufacturing are facing increasing threats to their cyber-physical systems. Safeguarding the identity of devices and machines as well as validating the possession of certificates and machines becomes even more relevant, once smart devices and smart machines are becoming “self-organizing” and need trustworthy and proven identities that they are able to authenticate with. 
