'''
	Implements @context attribute as defined in e.g. https://w3c.github.io/vc-data-model/#dfn-context and https://www.w3.org/TR/did-core/#example-12-an-example-of-a-relative-did-url !
'''


class Context:

    # > Constructor!
    def __init__(self, contexts):
        """Context constructor

        Args:
                contexts (str or list): May be a string or a list
        """
        if type(contexts) == type("str"):  # > May be a string or a list in DID Documents, but only a list on VCs, independently of number of elements!
            # > E.g. "https://www.w3.org/2018/credentials/v1" or "https://www.w3.org/ns/did/v1" !
            self._context = [contexts]
        else:
            self._context = contexts

    def toJSON(self, forceList=False):
        """Convert to JSON

        Args:
                forceList (bool, optional): Needs to be True if used on VC. Defaults to False.

        Returns:
                [json]: Returns context as json
        """
        if len(self._context) == 1:
            if forceList:
                return [self._context[0]]
            return self._context[0]
        else:
            return [e for e in self._context]
