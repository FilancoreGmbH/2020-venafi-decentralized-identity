'''
	Implements a Decentralized Identitifer (DID) Document following https://www.w3.org/TR/did-core/ !
'''

# > Dependencies!
import cryptographyWrapper
from PublicKey import PublicKey
from Did import Did
from Proof import Proof


class DidDocument():

    # > Constructor!
    def __init__(self, context, DIDObject, created, updated, publicKey, proof=None):
        """Constructor for the did document

        Args:
            context (Context): Context of the Did Document
            DIDObject (Did): Id which created the Did Document
            created (str): Timestamp when the Did document was created
            updated (str): Timestamp when the Did document was updated
            publicKey (PublicKey): Public key which will be used to verify the did document
            proof (Proof): Proof for the did document. Defaults to None.
        """

        self._context = context
        self._id = DIDObject
        self._created = created
        self._updated = updated
        self._publicKey = publicKey
        self._proof = proof

    def getContext(self):
        return self._context

    def getId(self):
        return self._id

    def getCreated(self):
        return self._created

    def getUpdated(self):
        return self._updated

    def getPublicKey(self):
        return self._publicKey

    def getPublicKeys(self):
        return [self._publicKey]

    def getProof(self):
        return self._proof

    def toJSON(self, ignoreFields=[]):
        """Converts all fields of the DID Document to JSON, except the ones passed in the ignoreFields list (e.g. for verification)!

        """

        data = {
            "@context": self.getContext(),
            "id": self.getId(),
            "created": self.getCreated(),
            "updated": self.getUpdated(),
            "publicKey": self.getPublicKey()
        }
        if self.getProof() is not None:
            data["proof"] = self.getProof()

        json = {}
        for key, value in data.items():
            if not key in ignoreFields:
                if key == "id":
                    json[key] = data[key].toString()
                elif key in ["@context", "proof"]:
                    json[key] = data[key].toJSON()
                elif key in ["publicKey"]:
                    json[key] = []
                    # for e in data[key].toJSON:
                    json[key].append(value.toJSON())
                else:
                    json[key] = data[key]
        return json

    # > Add given proof to DID Document!
    def addProof(self, p):
        """Adds Proof to a Did Document

        Args:
            p (Proof): Proof which will be added 
        """
        self._proof = p

    # > Verify DID Document's proof signature!
    def verify(self):
        """Verify Did Document's proof signature

        Returns:
            boolean: Return True if the Proof is correct
        """
        return cryptographyWrapper.verify(self.toJSON(["proof"]), self.getPublicKey().getPublicKey(), self.getProof().getSignatureValue())
