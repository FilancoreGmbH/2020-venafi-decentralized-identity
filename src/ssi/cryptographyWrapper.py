'''
	Implements a wrapper for cryptographic functions. Depends on https://www.pycryptodome.org/ and https://pynacl.readthedocs.io/ !
'''

# > Dependencies!
from Crypto.PublicKey import RSA
from Crypto.Signature import pkcs1_15
from Crypto.Hash import SHA384
import json
from base64 import b64encode, b64decode
from nacl.signing import SigningKey, VerifyKey
from nacl.encoding import HexEncoder
from nacl.exceptions import BadSignatureError
import base58


def generateKeyPair(type="rsa", bits=2048):
    """Creates a public/private keypair, either ed25519 or RSA. Bits are only relevant for RSA. Returns dict with keys!

    Args:
        type (str): Type which key should be generated. Either rsa or ed25199. Defaults to "rsa".
        bits (int, optional): Bits which are used in rsa. Defaults to 2048.

    Returns:
        [dict]: Returns a dict with public and private Key in either base58 for ed25519 or PEM for rsa
    """
    if type == "ed25519":
        privateKey = SigningKey.generate()
        publicKey = privateKey.verify_key
        return {"privateKey": str(base58.b58encode(privateKey.encode()), "utf-8"), "publicKey": str(base58.b58encode(publicKey.encode()), "utf-8")}
    elif type == "rsa":
        # > FIPS standard only defines 1024, 2048 and 3072 bits!
        keys = RSA.generate(bits)
        privateKey = keys.export_key(
            format='PEM', passphrase=None, pkcs=1, protection=None, randfunc=None)
        publicKey = keys.publickey().export_key(
            format='PEM', passphrase=None, protection=None, randfunc=None)
        # > Return keys as utf-8 strings!
        return {"publicKey": str(publicKey.decode("utf-8")), "privateKey": str(privateKey.decode("utf-8"))}


# >
def sign(JSON, privateKey):
    """Takes a JSON object and utf-8 string private key, returns the signature as utf-8 string!

    Args:
        JSON (json): JSON object which will be signed.
        privateKey (str): Private key which will be used for signing

    Returns:
        signature (str): Signature as a base 64 string
    """
    keyType = detectKeyType(privateKey)

    if keyType == "rsa":
        signer = pkcs1_15.new(RSA.import_key(privateKey, passphrase=None))
        digest = SHA384.new()
        # > Encode JSON to string, then to bytes!
        js = bytes(json.dumps(JSON), 'utf-8')
        digest.update(js)
        signature = signer.sign(digest)
        return b64encode(signature).decode("utf-8")
    else:
        sk = SigningKey(base58.b58decode(bytes(privateKey, "utf-8")))
        js = bytes(json.dumps(JSON), "utf-8")
        signature = sk.sign(js).signature
        return b64encode(signature).decode("utf-8")


def verify(JSON, publicKey, signature):
    """Verifies the validity of a JSON object's signature through the public key. Returns boolean indicating validity!

    Args:
        JSON (json): JSON object which needs to be verified
        publicKey (str): Public key which should be used
        signature (str): Signature which is used to verify the JSON

    Returns:
        [boolean]: Returns True is signature is valid, else False
    """
    keyType = detectKeyType(publicKey)

    if keyType == "rsa":
        signature = b64decode(signature.encode("utf-8"))
        verifier = pkcs1_15.new(RSA.import_key(publicKey, passphrase=None))
        digest = SHA384.new()
        # > Encode JSON to string, then to bytes!
        js = bytes(json.dumps(JSON), 'utf-8')
        digest.update(js)
        try:
            # > Raises a ValueError: Invalid signature if the signature is not valid!
            valid = verifier.verify(digest, signature)
            return True
        except ValueError:
            return False
    else:
        signature = b64decode(signature.encode("utf-8"))
        verifyingKey = VerifyKey(base58.b58decode(bytes(publicKey, "utf-8")))
        js = bytes(json.dumps(JSON), "utf-8")
        temp = verifyingKey.verify(js, signature)
        try:
            # > Raises a BadSignatureError: Invalid signature if the signature is not valid!
            temp = verifyingKey.verify(js, signature)
            return True
        except BadSignatureError:
            return False


def getPublicKeyfromPrivateKey(privateKeyPem):
    """Takes a private (RSA) key PEM and returns its public key!

    Args:
        privateKeyPem (str): Private key in PEM format

    Returns:
        [dict]: Returns a dict with public & private Key
    """
    keys = RSA.import_key(privateKeyPem)
    privateKey = keys.export_key(
        format='PEM', passphrase=None, pkcs=1, protection=None, randfunc=None)
    publicKey = keys.publickey().export_key(
        format='PEM', passphrase=None, protection=None, randfunc=None)
    # > Return keys as utf-8 strings!
    return {"publicKey": str(publicKey.decode("utf-8")), "privateKey": str(privateKey.decode("utf-8"))}


def detectKeyType(key):
    """Detect if a RSA or Ed25519 key is used

    Args:
        key (str): Key which should be analyzed as a string

    Returns:
        [str]: Returns either rsa for a RSA key otherwise ed25199 for a ed25199 key
    """
    if "BEGIN" in key:
        return "rsa"
    else:
        return "ed25519"
