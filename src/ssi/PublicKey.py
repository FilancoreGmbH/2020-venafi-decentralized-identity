'''
	Implements a publicKey object in the context of Decentralized Identitifer (DID) following https://www.w3.org/TR/did-core/ !
	See also https://www.w3.org/TR/did-core/#example-15-various-public-keys . Note: This is _not_ an arbitrary public key, but the DID Document Object/field!
	Note also that this can also be used for the "authentication" part of a DID Document!
'''

# > Dependencies!
from Did import Did
from PublicKeyReference import PublicKeyReference


class PublicKey:

    # > Constructor!
    def __init__(self, publicKeyCryptoObject, constrollerDidObject, didObject, suffix, type="RsaVerificationKey2018", seperator="#"):
        """Constructor PublicKey class
        Id will be a PublicKeyReference class 

        Args:
            publicKeyCryptoObject (PublicKey): Publickey as a crypto object
            constrollerDidObject (Did): Controller of key's DID Object
            didObject (Did): Did which is used in the PublicKeyReference
            suffix (str): Suffix which will be used in the PublicKeyReference
            type (str, optional): Type which cryptographic key is used. Defaults to "RsaVerificationKey2018".
            seperator (str, optional): Seperator which is used in the Public key reference. Defaults to "#".
        """
        self._id = PublicKeyReference(didObject, suffix, seperator)
        self._type = type  # > E.g. "Ed25519VerificationKey2018" or "RsaVerificationKey2018"!
        self._controller = constrollerDidObject  # > Controller of key's DID Object!
        self._publicKey = publicKeyCryptoObject

    def getId(self):
        return self._id

    def getType(self):
        return self._type

    def getController(self):
        return self._controller

    def getPublicKey(self):
        return self._publicKey

    @staticmethod
    def fromJSON(j):
        """Converts JSON to a public key object!

        Args:
            j (json): json which is used to generate a public key

        Returns:
            [PublicKey]: Generated public key 
        """
        
        splitID = j["id"].split("#")
        if j["publicKeyPem"] is not None:
            return PublicKey(j["publicKeyPem"], j["controller"], j["type"], Did(splitID[0]), splitID[1], "#")
        else:
            return PublicKey(j["publicKeyBase58"], j["controller"], j["type"], Did(splitID[0]), splitID[1], "#")

    def getIdstr(self):
        """Returns ID of current PublicKey

        Returns:
            str: ID in string format
        """
        return self._id._did.toString() + self._id._sep + self._id._suf


    def toJSON(self):
        """Converts this class to JSON!

        Returns:
            json: Json which was made out of the class
        """
        if "BEGIN" in self.getPublicKey():
            return {
                "id": self._id._did.toString() + self._id._sep + self._id._suf,
                "type": self._type,
                "controller": self._controller.toString(),
                "publicKeyPem": self._publicKey
            }
        else:
            return {
                "id": self._id._did.toString() + self._id._sep + self._id._suf,
                "type": self._type,
                "controller": self._controller.toString(),
                "publicKeyBase58": self._publicKey
            }
