'''
	Implements DID Document Proof field for holding a signature!
'''

# > Dependencies!
import datetime


class Proof:

    # > Constructor!
    def __init__(self, signatureValue, creator, proofPurpose=None, created=None, type="RsaVerificationKey2018"):
        """Generates a new Proof object

        Args:
            signatureValue (str): The signature value string encoded in base64
            creator (PublicKey): The PublicKey object which can be used to verify the signature value
            proofPurpose (str): Needs to be set if Proof is for a VC. Defaults to None.
            created (timestamp): Time when the Proof was created. Defaults to None.
            type (str): Parameter which key type was used.E.g. "RsaVerificationKey2018" or "Ed25519VerificationKey2018"!. Defaults to "RsaVerificationKey2018".
        """

        self._signatureValue = signatureValue
        self._creator = creator
        self._type = type
        if created is None:
            # > Timestamp following https://www.w3.org/TR/NOTE-datetime and ISO 8601!
            self._created = str(datetime.datetime.now())
        else:
            self._created = created
        self._proofPurpose = proofPurpose

    # >Getters!

    def getSignatureValue(self):
        return self._signatureValue

    def getCreator(self):
        return self._creator

    def getType(self):
        return self._type

    def getCreated(self):
        return self._created

    def getProofPurpose(self):
        return self._proofPurpose
    # > Convert to JSON!

    # >Setters!
    def setSignatureValue(self, v):
        self._signatureValue = v

    def setCreator(self, v):
        self._creator = v

    def setType(self, v):
        self._type = v

    def setCreated(self, v):
        self._created = v

    def setProofPurpose(self, v):
        self._proofPurpose = v

    def fromJson(self, json):
        """ Generates a Proof object out of a json

        Args:
            json (json): a Proof in json format

        Returns:
            (Proof): returns a new Proof object
        """
        return Proof(json['signatureValue'], json["creator"], None, json["created"], json["type"])

    def toJSON(self):
        """Return a Proof in json format

        Returns:
            json: Proof in json format
        """
        if self._proofPurpose is None:
            return {
                "type": self._type,
                "created": self._created,
                "creator": self._creator.toString(),
                "signatureValue": self._signatureValue
            }
        else:
            return {
                "type": self._type,
                "created": self._created,
                "proofPurpose": self._proofPurpose,
                "verificationMethod": self._creator,
                "jws": self._signatureValue
            }
