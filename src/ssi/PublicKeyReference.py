'''
	Implements a reference to a PublicKey object!
'''

from Did import Did


class PublicKeyReference:

    def __init__(self, did, suf, sep="#"):
        self._did = did
        self._suf = suf
        self._sep = sep

    @staticmethod
    def fromString(self, s, sep='#'):
        spl = s.split(sep)
        return PublicKeyReference(Did.fromString(), spl[2], sep)

    @staticmethod
    def fromJson(self, json, sep):
        return PublicKeyReference.fromString(json, sep)

    def getDid(self):
        return self._did

    def getSuffix(self):
        return self._suf

    def getSeperator(self):
        return self._sep

    def setDid(self, v):
        self._did = v

    def setSuffix(self, v):
        self._suf = v

    def setSeperator(self, v):
        self._sep = v

    def toString(self):
        return str(self.getDid().toString()+self.getSeperator()+self.getSuffix())

    def toJSON(self):
        return self.toString()

    def resolve(self, DidDocumentObj):
        for pko in DidDocumentObj.getPublicKeys():
            if pko.getIdstr() == self.toString():
                return pko
        return None
