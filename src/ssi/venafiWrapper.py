'''
    Wrapper to connect to the VENAFI (Cloud) Platform to fetch certificates!
'''

# > Dependencies!
from vcert import (CertificateRequest, Connection, CloudConnection, KeyType)
from asn1crypto import x509, pem

# > Standard libraries!
import time
import json

# > Fetches a new certificate through VENAFI cloud!
def fetchCertificate():
    """Fetches a certificate from venafi platform. config needs to be set to continue. 
    connectionType must be either tpp or cloud.
    if connectionType is cloud, cloud_token needs to be set .
    otherwise url, user, password needs to be set.
    zone always needs to be set.

    Returns:
        [type]: the fetched certificate
    """
 # > Set credentials!
    config = readConfig()
    connectionType= config["connectionType"]
    url = config["url"]
    user = config["user"]
    password = config["password"]
    cloud_token= config["cloud_token"] 
    zone = config["zone"]

    if connectionType == "cloud":
        # > Create connection and request!
        c = Connection(url=None, token=cloud_token, user=None,
                       password=None, http_request_kwargs={"verify": False})
    elif connectionType == "tpp":
        c = Connection(url=url, token=None, user=user,
                       password=password, http_request_kwargs={"verify": False})
    else:
        print("Please change connectionType to either tpp or cloud")
        return False

    r = CertificateRequest(common_name="filancore.venafi.example.com")
    r.san_dns = ["www.client.venafi.example.com",
                 "ww1.client.venafi.example.com"]
    if not isinstance(c, CloudConnection):
        r.email_addresses = ["e1@venafi.example.com", "e2@venafi.example.com"]
        r.ip_addresses = ["127.0.0.1", "192.168.1.1"]

    # > Config!
    r.key_type = KeyType(KeyType.RSA, 4096)
    zone_config = c.read_zone_conf(zone)
    r.update_from_zone_config(zone_config)

    # > Request certificate!
    c.request_cert(r, zone)

    # > Wait for signing!
    t = time.time() + 300
    while time.time() < t:
        cert = c.retrieve_cert(r)
        if cert:
            break
        else:
            time.sleep(5)

    # > Return certificate and key!
    return {"certificate": cert.full_chain, "privateKeyPem": r.private_key_pem}

# > Extracts certificate metadata!


def getCertificateMetadata(certBytes):
    """Gets all the certificate metadata

    Args:
        certBytes (bytes): certificate bytes 

    Returns:
        [json]: returns the metadata as a json 
    """
    if pem.detect(certBytes):
        typeName, headers, certBytes = pem.unarmor(certBytes)
    cert = x509.Certificate.load(certBytes)
    # > Default to string for datetime conversion!
    return json.loads(json.dumps(cert.native["tbs_certificate"], default=str))


def readConfig():
    try:
        with open('../config.json') as json_file:
            data = json.load(json_file)
            return data
    except:
        print("Please make a config file")
    
