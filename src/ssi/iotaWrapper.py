'''
    Implements a wrapper for IOTA DLT Operations!
'''

# > Dependencies!
from iota import Iota, TryteString, Tag, ProposedTransaction, Address, Transaction, Bundle
import json


def toTrytes(s):
    """Converts a string to trytes!

    Args:
            s (str): String which should be converted

    Returns:
            (str): Converted string in trytes
    """
    return (TryteString.from_unicode(s)).__str__()


def toAddr(s):
    """Converts a string to an IOTA address!

    Args:
        s (str): String which should be converted

    Returns:
        [str]: IOTA address as a string
    """
    s = toTrytes(s)  # > Convert to trytes!
    s = (s[:81]) if len(s) > 81 else s  # > Truncate to 81 characters!
    s = s.ljust(81, '9')  # > Fill to 81 characters if too short!
    return s


def publicKeyPEMToAddr(pem):
    """Takes a public key in PEM format and creates an address from it (removes leading and trailing static characters)

    Args:
        pem (str): Public key in PEM format string

    Returns:
        [str]: IOTA address in string format
    """
    return toAddr(pem[27+40:-25])


def publicKeyToAddr(key):
    """Takes a public key and creates an address from it (removes leading and trailing static characters)!

    Args:
        key (str): Public key in string format

    Returns:
        (str): IOTA address in string format
    """

    if "BEGIN" in key:
        return toAddr(key[27+40:-25])
    else:
        return toAddr(key)



def upload(JSON, target, adapter="https://nodes.thetangle.org:443", tagstr="", mmw=10, debugPrint=False, retries=3):
    """# > Uploads some data to the tangle. Will construct an address from target (e.g. a publicKey)!

    Args:
        JSON (json): Data which will be uploaded to the Tangle.
        target (str): IOTA Address where the transaction destinates.
        adapter (str, optional): The url where the transaction should be sent to . Defaults to "https://nodes.thetangle.org:443".
        tagstr (str, optional): The TAG which should be used in the transaction. Defaults to "".
        mmw (int, optional): MMW which should be used for the transaction. Defaults to 10.
        debugPrint (bool, optional): Boolean if the debug print should be shown. Defaults to False.
        retries (int, optional): Number of tries, if the transaction fails . Defaults to 3.

    Returns:
        [type]: Returns False if everything went fine
    """
    for i in range(retries):  # > Tries again on (e.g. node) error!

        try:
            # > Create api and config!
            if debugPrint:
                print("Creating api ...")
            JSONT = toTrytes(json.dumps(JSON))
            api = Iota(adapter=adapter)  # > Create API object!
            # target = toAddr(target)

            # > Prepare a transaction object!
            if debugPrint:
                print("Creating proposed transaction ...")
            tx = ProposedTransaction(
                address=Address(target),  # > Create address from target!
                value=0,
                tag=Tag(tagstr),
                message=JSONT,
            )

            if debugPrint:
                print("Preparing transfer ...")
            preparedTransfer = api.prepare_transfer([tx])

            if debugPrint:
                print("Get transactions to approve ...")
            gtta = api.get_transactions_to_approve(2)

            if debugPrint:
                print("Attaching to Tangle (PoW) ...")
            attached = api.attach_to_tangle(
                gtta["trunkTransaction"], gtta["branchTransaction"], preparedTransfer["trytes"], mmw)

            # > Send the transaction to the IOTA network!
            if debugPrint:
                print("Broadcasting ...")
            broadcast = api.broadcast_transactions(attached["trytes"])
            if debugPrint:
                print("	Address: " + str(target) + " !")
                print(
                    "	Direct link (comnet): https://comnet.thetangle.org/address/" + str(target) + " !")
                print(
                    "	Direct link (mainnet): https://thetangle.org/address/" + str(target) + " !")

            return {"address": str(target)}

        except Exception as e:
            if debugPrint:
                print("Error uploading ...")
                print(e)

    return False




def download(address, adapter="https://nodes.thetangle.org:443", retries=3):
    """Downloads some data from an address from the tangle!

    Args:
        address (str): IOTA address from where the data should download
        adapter (str, optional): Node url. Defaults to "https://nodes.thetangle.org:443".
        retries (int, optional): Number of retries, if an error occurs. Defaults to 3.

    Returns:
        [type]: Data from the Tangle. Return False if failed.
    """
    for i in range(retries):  # > Tries again on (e.g. node) error!

        try:
            api = Iota(adapter)
            add = Address(address)
            objs = api.find_transaction_objects(addresses=[add])
            # > This assumes one bundle per address, which is fine if RNG is properly seeded!
            return Bundle(objs["transactions"]).get_messages()[0]

        except Exception as e:
            if debugPrint:
                print("Error uploading ...")
                print(e)

    return False
