'''
	Implements a Decentralized Identitifer (DID) following https://www.w3.org/TR/did-core/ !
'''
# > Dependencies!
import iotaWrapper


class Did:

    # > Constructor!
    def __init__(self, uuid, network="main", method="iota", standard="did", seperator=":"):
        """Constructor of the DID class

        Args:
            uuid (str): The uuid string, following w3c standard
            network (str, optional): Specifies which network is used. Defaults to "main".
            method (str, optional): Specifies which method is used. Defaults to "iota".
            standard (str, optional): Specifies which standard is used. Defaults to "did".
            seperator (str, optional): Specifies which seperator is used. Defaults to ":".
        """
        # > Check for construction out of an existing (full) did string!
        if seperator in uuid:
            self._seperator = seperator
            parts = uuid.split(seperator)
            self._standard = parts[0]
            self._method = parts[1]

            # > Network is optional, so we need to check for that and default!
            if (len(parts) == 4):
                self._network = parts[2]
                self._uuid = parts[3]
            else:
                self._network = network
                self._uuid = parts[2]

        # > Else just construct normally!
        else:
            self._standard = standard
            self._method = method
            self._network = network
            self._uuid = uuid
            self._seperator = seperator

    def getUuid(self):
        return self._uuid

    def getNetwork(self):
        return self._network

    def getMethod(self):
        return self._network

    def getStandard(self):
        return self._standard

    def getSeperator(self):
        return self._seperator


    def toString(self):
        """Creates a string representation out of the DID!

        """
        return self._standard + self._seperator + self._method + self._seperator + self._network + self._seperator + self._uuid
