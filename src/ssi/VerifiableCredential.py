'''
	Implements a Verifiable Credential!
'''
import cryptographyWrapper as cryptoWr
from PublicKeyReference import PublicKeyReference
from Proof import Proof
import datetime


class VerifiableCredential():

    def __init__(self, issuerDIDOBJ, subjectDIDOBJ, claims, context="https://www.w3.org/2018/credentials/v1", id="1234", type=["VerifiableCredential"], issuanceDate=None, proof=None):
        """Constructor verifiable Credential

        Args:
            issuerDIDOBJ (Did): Entity that issued the credential!
            subjectDIDOBJ (Did): Subject which issues the credential 
            claims (str): Claims which are defined in the verifiable credential
            context (str, optional): Context definition. Defaults to "https://www.w3.org/2018/credentials/v1".
            id (str, optional): Identifier for the credential!. Defaults to "1234".
            type (list, optional): Declare what data to expect in the credential.. Defaults to ["VerifiableCredential"].
            issuanceDate (str, optional): When the credential was issued Defaults to None.
            proof (Proof): The Proof of the VC. Defaults to None.
        """
        credentialSubject = {
            "id": subjectDIDOBJ,
            "claims": claims
        }

        self._context = context
        self._id = id
        self._type = type
        self._issuer = issuerDIDOBJ

        self._credentialSubject = credentialSubject
        self._proof = proof
        if issuanceDate is None:
            # > Timestamp following https://www.w3.org/TR/NOTE-datetime and ISO 8601!
            self._issuanceDate = str(datetime.datetime.now())
        else:
            self._issuanceDate = issuanceDate

    # > converts JSON from VC

    # > Converts VC to JSON!

    def toJSON(self, ignoreFields=[]):
        """Returns VC object as json

        Args:
            ignoreFields (list, optional): Fields which should be ignored in the output json. Defaults to [].

        Returns:
            (json): VC object as json
        """
        data = {
            "context": self.getContext(),
            "id": self.getId(),
            "type": self.getType(),
            "issuer": self.getIssuer(),
            "issuanceDate": self.getissuanceDate(),
            "credentialSubject": self.getCredentialSubject()
        }
        if self.getProof() is not None:
            data["proof"] = self.getProof()

        json = {}
        for key, value in data.items():
            if not key in ignoreFields:
                if key == "issuer":
                    json[key] = data[key].toString()
                elif key in ["proof"]:
                    json[key] = data[key].toJSON()
                else:
                    json[key] = data[key]

        return json

    # > getters!
    def getContext(self):
        return self._context

    def getId(self):
        return self._id

    def getType(self):
        return self._type

    def getIssuer(self):
        return self._issuer

    def getProof(self):
        return self._proof

    def getissuanceDate(self):
        return self._issuanceDate

    def getSubjectID(self):
        return self.getCredentialSubject()["id"]

    def getClaims(self):
        return self.getCredentialSubject()["claims"]

    def getCredentialSubject(self):
        return self._credentialSubject
    # > setter!

    def setContext(self, c):
        self._context = c

    def setId(self, c):
        self._id = c

    def setType(self, c):
        self._type = c

    def setIssuer(self, c):
        self._issuer = c

    def setIssuanceDate(self, c):
        self._issuanceDate = c

    def setSubjectId(self, c):
        self._credentialSubject['id'] = c

    def setClaims(self, c):
        self._credentialSubject['claims'] = c

    def addProof(self, privateKey, publicKeyReferenceIdentifier="key-1"):
        """Adds a Proof to the VC object

        Args:
            privateKey (str): Private Key which should be used to sign the VC
            publicKeyReferenceIdentifier (str): Identifier which should be used. Defaults to "key-1".
        """
        creatorPKR = PublicKeyReference(
            self.getIssuer(), publicKeyReferenceIdentifier)
        signatureValue = cryptoWr.sign(self.toJSON(["proof"]), privateKey)
        self._proof = Proof(
            signatureValue, creatorPKR, None, None, "assertionMethod")

    def verify(self, didDocumentObject):
        """Verify the VC

        Args:
            didDocumentObject (DidDocument): Did Document which will verify the VC

        Returns:
            [bool]: Returns True if everything worked
        """
        if self.getProof() is None:
            return False

        try:
            publicKeyObject = self.getProof(
            ).getCreator().resolve(didDocumentObject)
            signature = self.getProof().getSignatureValue()
            vcSinProof = self.toJSON(["proof"])
            return cryptoWr.verify(vcSinProof, publicKeyObject._publicKey, signature)

        except:
            print("Something went wrong while VC.verify")
            return False
        else:
            return False
